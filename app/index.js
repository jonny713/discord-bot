const express = require('express')
const router = express.Router()
const DiscordHandler = require('./discord-handler.js')

const config = require('./../config.js')
const { charCommand } = config.discord || '!'

const discordHandler = new DiscordHandler(config.discord.token, charCommand)

/**
  * Находит кнопку в списке доступных голосов, ближайшую по значению к среднему арифметическому всех проголосовавших
  * {
  * @param {Array} buttonsNames набор кнопок голосования
  * @param {Number} average     среднее арифметическое результатов голосования
  * @param {Number} order       выбор округления среднего арифметического в сторону ближайшего меньшего (1) или большего (-1) значения среди набора кнопок
  * }
*/
const getAverageButton = function(buttonsNames, average, order = -1) {
  if ( order == 0 ) {
    throw new Error('Недопустимое значение для order')
  }
  let averageButton = Infinity
  const sortedButtons = [...buttonsNames]
  sortedButtons.sort((a,b) => {
    return Math.sign(order)*(a - b)})
  for (let button of sortedButtons) {
    if (Math.abs(button - average) < Math.abs(averageButton - average)) {
      averageButton = button
    }
  }
  return averageButton
}


// Тестовая команда
discordHandler.command('пинг', (req, res) => {
  res.sendImage('https://cdn.discordapp.com/emojis/251068761912770571.png', `Pong`)
})

// Всякие веселые команды
discordHandler.regex(':hailthesun:', (req, res) => {
  res.sendImage('https://cdn.discordapp.com/emojis/312502681639976960.png')
})
discordHandler.regex('хм', (req, res) => {
  res.sendImage('https://images.emojiterra.com/twitter/512px/1f914.png')
})
discordHandler.regex('дай деняк', (req, res) => {
  res.sendImage('https://fun-stories.ru/public/articles/20210302/59e7635cf6c4212c7ad5a3e6188caa1c.jpg')
})
discordHandler.repeat('Пухляш', 2225, (req, res) => {
  let {
    author: { id }
  } = req

  res.send(`<@${id}>, хватит жрать!`)
})

// Покер
discordHandler.command('покер', (req, res) => {
  let {
    author: { username }
  } = req

  // создаем список проголосовавших
  const votes = {}
  // функция регистрации голоса/итогового подсчета голосования
  const getVauchers = (votes, isFinal) => {
    const voteList = Object.values(votes)

    // не итоговое голосавние. показываем имена всех проголосовавших на данный момент участников
    if (!isFinal) {
      return `ПОКЕР: проголосовавшие ${voteList.map(vote => vote.username).join(", ")}`
    }
    // итоговое голосование

    // никто не проголосовал
    if (voteList.length === 0) {
      return 'Никто не пришел. Смирись!'
    }

    // возвращаем поименный список всех проголосовавших (с указанием выбранного варианта)
    // и среднее арифметическое
    let average = voteList.reduce((prev, vote) => prev + vote.value, 0) / voteList.length
    let averageButton = getAverageButton(buttonsNames, average)
    return `ПОКЕР: ${voteList.map(vote => vote.username.toString() + ":" + vote.value).join(", ")}.
Средний результат: ${averageButton}`
  }

  // добавляем кнопку подсчета голосов
  res.addButton({ buttonName: 'голоса', style: 'PRIMARY' }, (collector, interaction) => {
    if (interaction.user.username === username) {
      collector.stop()
      return getVauchers(votes, true);
    }
  })

  // формируем набор кнопок
  const buttonsNames = [1,2,3,5,8,13,21]
  buttonsNames.forEach(name => res.addButton({ buttonName: name.toString() }, (collector, interaction) => {
    votes[interaction.user.id] = { value: Number.parseInt(name), username: interaction.user.username };
    return getVauchers(votes);
  }))

  res.sendButtonsGroup('ПОКЕР')
})

// Преобразование стимовских ссылок
discordHandler.regex(/https:\/\/store\.steampowered\.com\/app\/(\d+)\/?/, (req, res) => {
  let { params } = req
  let steamId = params[0]
  res.send(`steam://store/${steamId}`)
})


router.get('/', (req, res) => {
  res.send(`
    <h1>Discord commands:</h1>
    ${discordHandler.commands.filter(({ type }) => type == 'command').map(command => `<div>${command.name}</div>`).join('')}
  `)
})

module.exports = router
