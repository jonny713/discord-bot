const express = require('express')
const app = express()
const bodyParser = require('body-parser')

// // TODO: заменить на npm модуль
// const logger = require('./modules/log.js')({ lim: 1, showTrace: false })

const config = require('./config.js')

// Включаем парсер параметров запроса
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

// Запускаем прослушивание
let server = app.listen(config.port)
console.log(`Сервер запущен, порт ${config.port}`);
console.log(`CTRL+клик ${config.protocol||'http'}://${config.host||'localhost'}:${config.port}/`);
// logger.showTrace = false

// Подключаем маршруты
app.use(require(`./app`))
